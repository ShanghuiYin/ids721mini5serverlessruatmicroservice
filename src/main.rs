use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

// Define a struct to represent the request payload received by the Lambda function
#[derive(Deserialize)]
struct Request {
    gender: Option<String>,
    role: Option<String>,
}


// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    name: String,
    employee_id: Option<u64>,
}



// Function to search for leadership information in DynamoDB based on provided parameters
async fn search_leadership(client: &Client, gender: Option<String>, role: Option<String>) -> Result<(String, String), LambdaError> {
    let table_name = "AppleLeadership";
    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();
    let mut expr_attr_names = HashMap::new(); // Initialize expression attribute names map

    if let Some(gender_val) = gender {
        expr_attr_values.insert(":gender_val".to_string(), AttributeValue::S(gender_val));
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        filter_expression.push_str("gender = :gender_val");
    }

    if let Some(role_val) = role {
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        expr_attr_values.insert(":role_val".to_string(), AttributeValue::S(role_val));
        // Use an expression attribute name for "role"
        expr_attr_names.insert("#role_attr".to_string(), "role".to_string());
        filter_expression.push_str("#role_attr = :role_val"); // Adjusted to use the expression attribute name
    }

    let result = client.scan()
        .table_name(table_name)
        .set_expression_attribute_values(Some(expr_attr_values))
        .set_filter_expression(Some(filter_expression))
        .set_expression_attribute_names(Some(expr_attr_names)) // Include this in your request
        .send()
        .await?;

    // Extract the items from the scan result
    let items = result.items.unwrap_or_default();

    // Randomly choose an item from the scan result
    let selected_item = items.iter().choose(&mut thread_rng());

    // Match and extract relevant attributes from the selected item
    match selected_item {
        Some(item) => {
            let name_attr = item.get("name").and_then(|val| val.as_s().ok());
            let role_attr = item.get("role").and_then(|val| val.as_s().ok());

            // Match the extracted attributes and return the result
            match (name_attr, role_attr) {
                (Some(name), Some(role)) => Ok((name.to_string(), role.to_string())),
                _ => Err(LambdaError::from("No name or role found in the selected item")),
            }
        },
        None => Err(LambdaError::from("No matching leadership information found")),
    }
}


async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    // Deserialize the incoming JSON payload into the Request struct
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    // Rest of the function remains the same...
    let (name, role) = search_leadership(&client, request.gender, request.role).await?;

    // Serialize the response into JSON format and return it
    Ok(json!({
        "name": name,
        "role": role,
    }))
}


// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    // Create a service function using the handler function and run the Lambda function
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
