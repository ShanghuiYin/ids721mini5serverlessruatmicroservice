
# IDS721 Spring 2024 Weekly Mini Project 5 Serverless Rust Microservice

## Demo

- A simple Apple Leadership role lookup API. For example, search the CEO:

![alt text](image.png)

## Requirements
1. Create a Rust AWS Lambda function (or app runner)
2. Implement a simple service
3. Connect to a database

## Local Setup
1. Install Rust
2. Start by creating a new binary-based Cargo project and changing into the new directory:
   `cargo new serverless-rust-microservice && cd serverless-rust-microservice`
3. Implement the logic in `main.rs` file
4. Add the dependencies in `Cargo.toml` file
5. Build the project `cargo build --release`
6. Deploy the project `cargo deploy`

## AWS Setup

1. Attach the following policies to the role associated with the AWS Lambda function: AmazonDynamoDBFullAccess, AWSLambda_FullAccess, and AWSLambdaBasicExecutionRole.
![alt text](image-1.png)

2. Search for DynamoDB on the AWS console. Create a new table named `AppleLeadership`. Under Explore items, create some new items as data for the service.
![alt text](image-2.png)

| AppleLeadership intends to record all Apple Inc leadership roles with attributes
- Name (string)
- Gender (string)
- Role (string)


3. Under the Lambda function, create a RESTful API trigger.

4. Create a new resource for the REST API. Specify a resource name such as `mini5api`. Then add an ANY method to the resouce. Deploy the API by clicking the button on the top right. 
![alt text](image-3.png)

7. Invoke the service using the URL `https://avxlhdsnl9.execute-api.us-east-1.amazonaws.com/default/serverless-rust-microservice/mini5api` and [PostMan](https://www.postman.com/) which is an API platform for building and using APIs

- Success demo
![alt text](image.png)

- Failure Dmoe
![alt text](image-4.png)


## Reference
1. https://www.cargo-lambda.info/guide/getting-started.html
2. https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/rds-lambda-tutorial.html
3. https://www.apple.com/leadership/
4. https://www.postman.com/

